package br.com.mastertech.consumerConsultaEmpresa;
/* nome do pacote acima precisa ser identico ao que gerou a mensagem na fila */

public class CnpjDTO {
    private String nome;
    private String cnpj;
    private String capital_social;
    private String status;

    public CnpjDTO() {
    }

    public CnpjDTO(String nome, String cnpj, String capital_social, String status) {
        this.nome = nome;
        this.cnpj = cnpj;
        this.capital_social = capital_social;
        this.status = status;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(String capital_social) {
        this.capital_social = capital_social;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
