package br.com.mastertech.consumerArquivoConsultaEmpresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerArquivoConsultaEmpresaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerArquivoConsultaEmpresaApplication.class, args);
	}

}
