package br.com.mastertech.consumerArquivoConsultaEmpresa;

import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;

@Component
public class WriteToFile {

    public static void gravaLog(String log) {
        try {
            FileWriter myWriter = new FileWriter("ConsultaEmpresas.txt", true);
            myWriter.append(log);
            myWriter.append(System.getProperty("line.separator"));
            myWriter.flush();
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}



