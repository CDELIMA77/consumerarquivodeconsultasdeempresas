package br.com.mastertech.consumerArquivoConsultaEmpresa;

import br.com.mastertech.consumerConsultaEmpresa.CnpjDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ConsumerArquivoConsultaEmpresa {

    @Autowired
    WriteToFile writeToFile;

    @KafkaListener(topics = "spec2-cynthia-carvalho-3", groupId = "grupo-3")
    public void receber(@Payload CnpjDTO cnpjDTO) {
        String linhaLog ;
        linhaLog = "Consulta empresa "+ cnpjDTO.getNome() + " cujo CNPJ e " + cnpjDTO.getCnpj() + " com capital social de " + cnpjDTO.getCapital_social() + " foi " + cnpjDTO.getStatus();
        System.out.println(linhaLog);
        writeToFile.gravaLog(linhaLog);
    }
}
